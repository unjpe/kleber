#include <stdio.h>

int main(){
    int n;
    printf("Digite um ano: ");
    scanf("%d", &n);
    if(n % 4 == 0)
        printf("%d eh um ano bissexto\n", n);
    else
        printf("%d NAO eh um ano bissexto\n", n);
    return 0;
}