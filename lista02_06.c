#include <stdio.h>

int main(){
    int n;
    float aux = 1.0, q; //A razao de uma P.G. pode ser fracionaria, por isso optei por float.
    printf("Digite o numero de termos da P.G.: ");
    scanf("%d", &n);
    printf("Digite a razao da P.G.: ");
    scanf("%f", &q);
    printf("\nOs %d elementos da P.G. com razao %.4f, com valor inicial um, sao: \n", n, q);
    for(int i = 0; i < n; i++){
        printf("%.4f\n", aux);   //como queremos comecar do um, imprimimos antes de incrementar.
        aux *= q;
    }
    return 0;
}
