#include <stdio.h>

int main(){
    int n;
    printf("Digite um numero inteiro n: ");
    scanf("%d", &n);
    if(n % 2 == 0)
        printf("n = %d eh par", n);
    else
        printf("n = %d eh impar", n);
    return 0;
}