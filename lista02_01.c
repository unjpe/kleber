#include <stdio.h>
#include <stdlib.h>

int main(){
    char x = 1;
    while(x != 0){
        printf("=======================================\n");
        printf("FAST JUNK'S - DIGITE A OPCAO DESEJADA: \n");
        printf("=======================================\n\n");
        printf("Opcao 1 - BATATA FRITA: \n");
        printf("Opcao 2 - HAMBURGER: \n");
        printf("Opcao 3 - X-BURGER: \n");
        printf("Opcao 4 - HOT-DOG: \n");
        printf("Opcao 5 - COXINHA: \n");
        printf("Opcao 6 - PASTEL: \n");
        printf("Opcao 7 - AGUA: \n");
        printf("Opcao 8 - SUCO: \n");
        printf("Opcao 9 - REFRIGERANTE: \n");
        printf("0 - SAIR: \n\n");
        scanf("%d", &x);
        system("cls");
        switch(x){
            case 1:
                printf("Voce escolheu BATATA FRITA!\n\n");
                break;
            case 2:
                printf("Voce escolheu HAMBURGER!\n\n");
                break;
            case 3:
                printf("Voce escolheu X-BURGER!\n\n");
                break;
            case 4:
                printf("Voce escolheu HOT-DOG!\n\n");
                break;
            case 5:
                printf("Voce escolheu COXINHA!\n\n");
                break;
            case 6:
                printf("Voce escolheu PASTEL!\n\n");
                break;
            case 7:
                printf("Voce escolheu AGUA!\n\n");
                break;
            case 8:
                printf("Voce escolheu SUCO!\n\n");
                break;
            case 9:
                printf("Voce escolheu REFRIGERANTE!\n\n");
                break;
            case 0:
                printf("Volte sempre!\n");
                return 0;
            default:
                printf("Opcao invalida. Tente novamente!\n\n");
        }
    }
    return 0;
}
