#ifndef BETFUNCTIONS_H_INCLUDED
#define BETFUNCTIONS_H_INCLUDED

int funcaoPar(int num){
    if(num % 2 == 0 && num != 0){
        return 1;
    } else
        return 0;
}

int funcaoImpar(int num){
    if(num % 2 != 0){
        return 1;
    } else
        return 0;
}

int funcaoPrimeiros18(int num){
    if(num >= 1 && num <= 18)
        return 1;
    else
        return 0;
}

int funcaoSegundos18(int num){
    if(num >= 19 && num <= 36)
        return 1;
    else
        return 0;
}

int funcaoPrimeiraDuzia(int num){
    if(num >= 1 && num <= 12)
        return 1;
    else
        return 0;
}

int funcaoSegundaDuzia(int num){
    if(num >= 13 && num <= 24)
        return 1;
    else
        return 0;
}

int funcaoTerceiraDuzia(int num){
    if(num >= 25 && num <= 36)
        return 1;
    else
        return 0;
}


int funcaoPrimeiraLinha(int num){
    if(num % 3 == 1)
        return 1;
    else
        return 0;
}


int funcaoSegundaLinha(int num){
    if(num % 3 == 2)
        return 1;
    else
        return 0;
}

int funcaoTerceiraLinha(int num){
    if(num % 3 == 0)
        return 1;
    else
        return 0;
}

int funcaoCorPreta(int num){
    int i, flag = 0, v[18] = {2, 4 , 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35};
    for(i = 0; i < 18; i++){
        if(v[i] == num){
            flag = 1;
            break;
        }
    }

    if(flag == 1)
        return 1;
    else
        return 0;
}

int funcaoCorVermelha(int num){
    int i, flag = 0, v[18] = {1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36};
    for(i = 0; i < 18; i++){
        if(v[i] == num){
            flag = 1;
            break;
        }
    }

    if(flag == 1)
        return 1;
    else
        return 0;
}

#endif // BETFUNCTIONS_H_INCLUDED
