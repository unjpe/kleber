#ifndef PLAYERSFUNCTIONS_H_INCLUDED
#define PLAYERSFUNCTIONS_H_INCLUDED

struct cadastro_players{
    char nome[3+1];
    int fichas;
    int posicao;
}player[12], temp;

struct Banca
{
    int fichas;
}banca;


int criar_player(int numplayer){
    char nome[10];
    int fichas, i = numplayer;

    printf("Digite nome (3 letras): ");
    scanf("%s", &nome);
    do{
        printf("Digite fichas do player (20 - 75): ");
        scanf("%d", &fichas);
    }while(fichas < 20 || fichas > 75);

    strcpy(player[i].nome, nome);
    player[i].nome[3] = '\0';
    player[i].posicao = i;
    player[i].fichas = fichas;
    printf("Player cadastrado com sucesso!!!\n\n");
    printf("Nome do player:\t%s\n", player[i].nome);
    printf("fichas do player:\t%d\n\n", player[i].fichas);
}

int jogar1(){
    int num;
    int i = 0, jogar = 1, p1 = 0, p1soma = 0, p2soma = 0, continuar;
    int p1respnum = 0, p2respnum = 0, p1num = 0, p2num = 0, p1numabet = 0, p2numabet = 0;
    int p1par = 0, p1impar = 0, p1pri18 = 0, p1seg18 = 0,  p1pri12 = 0, p1seg12 = 0, p1ter12 = 0, p1prilin = 0, p1seglin = 0, p1terlin = 0, p1preto = 0, p1verm = 0;

    printf("Player 1: \n");
	criar_player(10);
    p1 = 10;


    do{
        printf("Rodada %d: ", i+1);

        printf("\nFichas da banca: %d\n", banca.fichas);
        printf("Fichas de %s: %d\n\n", player[p1].nome, player[p1].fichas);

        do{
            printf("Turno de %s\n", player[p1].nome);
            printf("Fichas apostadas em par: ");
            scanf("%d", &p1par);
            printf("Fichas apostadas em impar: ");
            scanf("%d", &p1impar);
            printf("Fichas apostadas em 1-18: ");
            scanf("%d", &p1pri18);
            printf("Fichas apostadas em 19-36: ");
            scanf("%d", &p1seg18);
            printf("Fichas apostadas em 01-12: ");
            scanf("%d", &p1pri12);
            printf("Fichas apostadas em 13-24: ");
            scanf("%d", &p1seg12);
            printf("Fichas apostadas em 25-36: ");
            scanf("%d", &p1ter12);
            printf("Fichas apostadas na primeira linha: ");
            scanf("%d", &p1prilin);
            printf("Fichas apostadas na segunda linha: ");
            scanf("%d", &p1seglin);
            printf("Fichas apostadas na terceira linha: ");
            scanf("%d", &p1terlin);
            printf("Fichas apostadas na cor preta: ");
            scanf("%d", &p1preto);
            printf("Fichas apostadas na cor vermelha: ");
            scanf("%d", &p1verm);

            printf("Deseja aposta em algum numero? (Sim - 1/ Nao - 0) ");
            scanf("%d", &p1respnum);

            if(p1respnum != 0){
                do{
                    printf("Qual numero (1 - 36)? ");
                    scanf("%d", &p1num);

                    if(p1num < 1 || p1num > 36)
                        printf("\nNumero nao permitido\n");

                }while(p1num < 1 || p1num > 36);

                printf("Quantas fichas? ");
                scanf("%d", &p1numabet);
            }


            p1soma = p1par + p1impar + p1pri18 + p1seg18 + p1pri12 + p1seg12 + p1ter12 + p1prilin + p1seglin + p1terlin + p1preto + p1verm + p1numabet;

            if(p1soma == 0)
                printf("\nAposta minima 1 ficha. Tente outra vez!\n\n");
            if(p1soma > player[p1].fichas)
                printf("\nVoce nao possui fichas suficientes. Tente outra vez!\n\n");

        }while(p1soma == 0 || p1soma > player[p1].fichas);



        srand(time(0));
        num = rand() % 37;

        system("cls");
        printf("Resultado da roleta: %d", num);

        //Paridade
        if(funcaoPar(num) == 1){
            player[p1].fichas += p1par;
            banca.fichas -= p1par;
        }
        else{
            player[p1].fichas -= p1par;
            banca.fichas += p1par;
        }

        if(funcaoImpar(num) == 1){
            player[p1].fichas += p1impar;
            banca.fichas -= p1impar;
        }
        else{
            player[p1].fichas -= p1impar;
            banca.fichas += p1impar;
        }

        //18
        if(funcaoPrimeiros18(num) == 1){
            player[p1].fichas += p1pri18;
            banca.fichas -= p1pri18;
        }
        else{
            player[p1].fichas -= p1pri18;
            banca.fichas += p1pri18;
        }

        if(funcaoSegundos18(num) == 1){
            player[p1].fichas += p1seg18;
            banca.fichas -= p1seg18;
        }
        else{
            player[p1].fichas -= p1seg18;
            banca.fichas += p1seg18;
        }

        //Duzias
        if(funcaoPrimeiraDuzia(num) == 1){
            player[p1].fichas += 2*p1pri12;
            banca.fichas -= 2*p1pri12;
        }
        else{
            player[p1].fichas -= 2*p1pri12;
            banca.fichas += 2*p1pri12;
        }

        if(funcaoSegundaDuzia(num) == 1){
            player[p1].fichas += 2*p1seg12;
            banca.fichas -= 2*p1seg12;
        }
        else{
            player[p1].fichas -= 2*p1seg12;
            banca.fichas += 2*p1seg12;
        }

        if(funcaoTerceiraDuzia(num) == 1){
            player[p1].fichas += 2*p1ter12;
            banca.fichas -= 2*p1ter12;
        }
        else{
            player[p1].fichas -= 2*p1ter12;
            banca.fichas += 2*p1ter12;
        }


        //Linhas
        if(funcaoPrimeiraLinha(num) == 1){
            player[p1].fichas += 2*p1prilin;
            banca.fichas -= 2*p1prilin;
        }
        else{
            player[p1].fichas -= 2*p1prilin;
            banca.fichas += 2*p1prilin;
        }

        if(funcaoSegundaLinha(num) == 1){
            player[p1].fichas += 2*p1seglin;
            banca.fichas -= 2*p1seglin;
        }
        else{
            player[p1].fichas -= 2*p1seglin;
            banca.fichas += 2*p1seglin;
        }

        if(funcaoTerceiraLinha(num) == 1){
            player[p1].fichas += 2*p1terlin;
            banca.fichas -= 2*p1terlin;
        }
        else{
            player[p1].fichas -= 2*p1terlin;
            banca.fichas += 2*p1terlin;
        }

        //Cores
        if(funcaoCorPreta(num) == 1){
            player[p1].fichas += p1preto;
            banca.fichas -= p1preto;
        }
        else{
            player[p1].fichas -= p1preto;
            banca.fichas += p1preto;
        }

        if(funcaoCorVermelha(num) == 1){
            player[p1].fichas += p1verm;
            banca.fichas -= p1verm;
        }
        else{
            player[p1].fichas -= p1verm;
            banca.fichas += p1verm;
        }

        //Numeros
        if(num == p1num){
            player[p1].fichas += 35*p1numabet;
            banca.fichas -= 35*p1numabet;
        }
        else{
            player[p1].fichas -= p1numabet;
            banca.fichas += p1numabet;
        }

        jogar = 1;
        i++;

        //Logica de fim de jogo
        if(player[p1].fichas == 0){
            jogar = 0;
        }


        if(banca.fichas < 0){
            banca.fichas = 0;
            jogar = 0;
        }

        printf("\nFichas da banca: %d", banca.fichas);
        printf("\nFichas de %s: %d", player[p1].nome, player[p1].fichas);

        printf("\n\nQuer continuar jogando?  (Sim - 1/ Nao - 0) ");
        scanf("%d", &continuar);
        system("cls");

        if(!continuar)
            jogar = 0;

       }while(jogar);

       atualiza_ranking();
}



int jogar2(){
    int num;
    int i = 0, jogar = 1, p1 = 0, p2 = 0, p1soma = 0, p2soma = 0, continuar;
    int p1respnum = 0, p2respnum = 0, p1num = 0, p2num = 0, p1numabet = 0, p2numabet = 0;
    int p1par = 0, p1impar = 0, p1pri18 = 0, p1seg18 = 0,  p1pri12 = 0, p1seg12 = 0, p1ter12 = 0, p1prilin = 0, p1seglin = 0, p1terlin = 0, p1preto = 0, p1verm = 0;
	int p2par = 0, p2impar = 0, p2pri18 = 0, p2seg18 = 0,  p2pri12 = 0, p2seg12 = 0, p2ter12 = 0, p2prilin = 0, p2seglin = 0, p2terlin = 0, p2preto = 0, p2verm = 0;

    printf("Player 1: \n");
	criar_player(10);
    p1 = 10;


    printf("\n");
    printf("Player 2: \n");
    criar_player(11);
    p2 = 11;

    do{
        printf("Rodada %d: ", i+1);

        printf("\nFichas da banca: %d\n", banca.fichas);
        printf("Fichas de %s: %d\n", player[p1].nome, player[p1].fichas);
        printf("Fichas de %s: %d\n\n", player[p2].nome, player[p2].fichas);

        do{
            printf("Turno de %s\n", player[p1].nome);
            printf("Fichas apostadas em par: ");
            scanf("%d", &p1par);
            printf("Fichas apostadas em impar: ");
            scanf("%d", &p1impar);
            printf("Fichas apostadas em 1-18: ");
            scanf("%d", &p1pri18);
            printf("Fichas apostadas em 19-36: ");
            scanf("%d", &p1seg18);
            printf("Fichas apostadas em 01-12: ");
            scanf("%d", &p1pri12);
            printf("Fichas apostadas em 13-24: ");
            scanf("%d", &p1seg12);
            printf("Fichas apostadas em 25-36: ");
            scanf("%d", &p1ter12);
            printf("Fichas apostadas na primeira linha: ");
            scanf("%d", &p1prilin);
            printf("Fichas apostadas na segunda linha: ");
            scanf("%d", &p1seglin);
            printf("Fichas apostadas na terceira linha: ");
            scanf("%d", &p1terlin);
            printf("Fichas apostadas na cor preta: ");
            scanf("%d", &p1preto);
            printf("Fichas apostadas na cor vermelha: ");
            scanf("%d", &p1verm);

            printf("Deseja aposta em algum numero? (Sim - 1/ Nao - 0) ");
            scanf("%d", &p1respnum);

            if(p1respnum != 0){
                do{
                    printf("Qual numero (1 - 36)? ");
                    scanf("%d", &p1num);

                    if(p1num < 1 || p1num > 36)
                        printf("\nNumero nao permitido\n");

                }while(p1num < 1 || p1num > 36);

                printf("Quantas fichas? ");
                scanf("%d", &p1numabet);
            }


            p1soma = p1par + p1impar + p1pri18 + p1seg18 + p1pri12 + p1seg12 + p1ter12 + p1prilin + p1seglin + p1terlin + p1preto + p1verm + p1numabet;

            if(p1soma == 0)
                printf("\nAposta minima 1 ficha. Tente outra vez!\n\n");
            if(p1soma > player[p1].fichas)
                printf("\nVoce nao possui fichas suficientes. Tente outra vez!\n\n");

        }while(p1soma == 0 || p1soma > player[p1].fichas);


        do{
            printf("\n\nTurno de %s\n", player[p2].nome);
            printf("Fichas apostadas em par: ");
            scanf("%d", &p2par);
            printf("Fichas apostadas em impar: ");
            scanf("%d", &p2impar);
            printf("Fichas apostadas em 1-18: ");
            scanf("%d", &p2pri18);
            printf("Fichas apostadas em 19-36: ");
            scanf("%d", &p2seg18);
            printf("Fichas apostadas em 01-12: ");
            scanf("%d", &p2pri12);
            printf("Fichas apostadas em 13-24: ");
            scanf("%d", &p2seg12);
            printf("Fichas apostadas em 25-36: ");
            scanf("%d", &p2ter12);
            printf("Fichas apostadas na primeira linha: ");
            scanf("%d", &p2prilin);
            printf("Fichas apostadas na segunda linha: ");
            scanf("%d", &p2seglin);
            printf("Fichas apostadas na terceira linha: ");
            scanf("%d", &p2terlin);
            printf("Fichas apostadas na cor preta: ");
            scanf("%d", &p2preto);
            printf("Fichas apostadas na cor vermelha: ");
            scanf("%d", &p2verm);

            printf("Deseja aposta em algum numero? (Sim - 1/ Nao - 0) ");
            scanf("%d", &p2respnum);

            if(p2respnum != 0){
                do{
                    printf("Qual numero (1 - 36)? ");
                    scanf("%d", &p2num);

                    if(p2num < 1 || p2num > 36)
                        printf("\nNumero nao permitido\n");

                }while(p2num < 1 || p2num > 36);

                printf("Quantas fichas? ");
                scanf("%d", &p2numabet);
            }

            p2soma = p2par + p2impar + p2pri18 + p2seg18 + p2pri12 + p2seg12 + p2ter12 + p2prilin + p2seglin + p2terlin + p2preto + p2verm + p2numabet;

            if(p2soma == 0)
                printf("\nAposta minima 1 ficha. Tente outra vez!\n");
            if(p2soma > player[p2].fichas)
                printf("\nVoce nao possui fichas suficientes. Tente outra vez!\n");

        }while(p2soma == 0 || p2soma > player[p2].fichas);


        srand(time(0));
        num = rand() % 37;

        system("cls");
        printf("Resultado da roleta: %d", num);

        //Paridade
        if(funcaoPar(num) == 1){
            player[p1].fichas += p1par;
            banca.fichas -= p1par;
        }
        else{
            player[p1].fichas -= p1par;
            banca.fichas += p1par;
        }

        if(funcaoImpar(num) == 1){
            player[p1].fichas += p1impar;
            banca.fichas -= p1impar;
        }
        else{
            player[p1].fichas -= p1impar;
            banca.fichas += p1impar;
        }

        //18
        if(funcaoPrimeiros18(num) == 1){
            player[p1].fichas += p1pri18;
            banca.fichas -= p1pri18;
        }
        else{
            player[p1].fichas -= p1pri18;
            banca.fichas += p1pri18;
        }

        if(funcaoSegundos18(num) == 1){
            player[p1].fichas += p1seg18;
            banca.fichas -= p1seg18;
        }
        else{
            player[p1].fichas -= p1seg18;
            banca.fichas += p1seg18;
        }

        //Duzias
        if(funcaoPrimeiraDuzia(num) == 1){
            player[p1].fichas += 2*p1pri12;
            banca.fichas -= 2*p1pri12;
        }
        else{
            player[p1].fichas -= p1pri12;
            banca.fichas += p1pri12;
        }

        if(funcaoSegundaDuzia(num) == 1){
            player[p1].fichas += 2*p1seg12;
            banca.fichas -= 2*p1seg12;
        }
        else{
            player[p1].fichas -= p1seg12;
            banca.fichas += p1seg12;
        }

        if(funcaoTerceiraDuzia(num) == 1){
            player[p1].fichas += 2*p1ter12;
            banca.fichas -= 2*p1ter12;
        }
        else{
            player[p1].fichas -= p1ter12;
            banca.fichas += p1ter12;
        }


        //Linhas
        if(funcaoPrimeiraLinha(num) == 1){
            player[p1].fichas += 2*p1prilin;
            banca.fichas -= 2*p1prilin;
        }
        else{
            player[p1].fichas -= p1prilin;
            banca.fichas += p1prilin;
        }

        if(funcaoSegundaLinha(num) == 1){
            player[p1].fichas += 2*p1seglin;
            banca.fichas -= 2*p1seglin;
        }
        else{
            player[p1].fichas -= p1seglin;
            banca.fichas += p1seglin;
        }

        if(funcaoTerceiraLinha(num) == 1){
            player[p1].fichas += 2*p1terlin;
            banca.fichas -= 2*p1terlin;
        }
        else{
            player[p1].fichas -= p1terlin;
            banca.fichas += p1terlin;
        }

        //Cores
        if(funcaoCorPreta(num) == 1){
            player[p1].fichas += p1preto;
            banca.fichas -= p1preto;
        }
        else{
            player[p1].fichas -= p1preto;
            banca.fichas += p1preto;
        }

        if(funcaoCorVermelha(num) == 1){
            player[p1].fichas += p1verm;
            banca.fichas -= p1verm;
        }
        else{
            player[p1].fichas -= p1verm;
            banca.fichas += p1verm;
        }

        //Numeros
        if(num == p1num){
            player[p1].fichas += 35*p1numabet;
            banca.fichas -= 35*p1numabet;
        }
        else{
            player[p1].fichas -= p1numabet;
            banca.fichas += p1numabet;
        }

        //P2
        //Paridade
        if(funcaoPar(num) == 1){
            player[p2].fichas += p2par;
            banca.fichas -= p2par;
        }
        else{
            player[p2].fichas -= p2par;
            banca.fichas += p2par;
        }

        if(funcaoImpar(num) == 1){
            player[p2].fichas += p2impar;
            banca.fichas -= p2impar;
        }
        else{
            player[p2].fichas -= p2impar;
            banca.fichas += p2impar;
        }

        //18
        if(funcaoPrimeiros18(num) == 1){
            player[p2].fichas += p2pri18;
            banca.fichas -= p2pri18;
        }
        else{
            player[p2].fichas -= p2pri18;
            banca.fichas += p2pri18;
        }

        if(funcaoSegundos18(num) == 1){
            player[p2].fichas += p2seg18;
            banca.fichas -= p2seg18;
        }
        else{
            player[p2].fichas -= p2seg18;
            banca.fichas += p2seg18;
        }

        //Duzias
        if(funcaoPrimeiraDuzia(num) == 1){
            player[p2].fichas += 2*p2pri12;
            banca.fichas -= 2*p2pri12;
        }
        else{
            player[p2].fichas -= p2pri12;
            banca.fichas += p2pri12;
        }

        if(funcaoSegundaDuzia(num) == 1){
            player[p2].fichas += 2*p2seg12;
            banca.fichas -= 2*p2seg12;
        }
        else{
            player[p2].fichas -= p2seg12;
            banca.fichas += p2seg12;
        }

        if(funcaoTerceiraDuzia(num) == 1){
            player[p2].fichas += 2*p2ter12;
            banca.fichas -= 2*p2ter12;
        }
        else{
            player[p2].fichas -= p2ter12;
            banca.fichas += p2ter12;
        }


        //Linhas
        if(funcaoPrimeiraLinha(num) == 1){
            player[p2].fichas += 2*p2prilin;
            banca.fichas -= 2*p2prilin;
        }
        else{
            player[p2].fichas -= p2prilin;
            banca.fichas += p2prilin;
        }

        if(funcaoSegundaLinha(num) == 1){
            player[p2].fichas += 2*p2seglin;
            banca.fichas -= 2*p2seglin;
        }
        else{
            player[p2].fichas -= p2seglin;
            banca.fichas += p2seglin;
        }

        if(funcaoTerceiraLinha(num) == 1){
            player[p2].fichas += 2*p2terlin;
            banca.fichas -= 2*p2terlin;
        }
        else{
            player[p2].fichas -= p2terlin;
            banca.fichas += p2terlin;
        }

        //Cores
        if(funcaoCorPreta(num) == 1){
            player[p2].fichas += p2preto;
            banca.fichas -= p2preto;
        }
        else{
            player[p2].fichas -= p2preto;
            banca.fichas += p2preto;
        }

        if(funcaoCorVermelha(num) == 1){
            player[p2].fichas += p2verm;
            banca.fichas -= p2verm;
        }
        else{
            player[p2].fichas -= p2verm;
            banca.fichas += p2verm;
        }

        //Numeros
        if(num == p2num){
            player[p2].fichas += 35*p2numabet;
            banca.fichas -= 35*p2numabet;
        }
        else{
            player[p2].fichas -= p2numabet;
            banca.fichas += p2numabet;
        }

        jogar = 1;
        i++;

        //Logica de fim de jogo
        if(player[p1].fichas == 0){
            jogar = 0;
        }

        if(player[p2].fichas == 0){
            jogar = 0;
        }

        if(banca.fichas < 0){
            banca.fichas = 0;
            jogar = 0;
        }

        printf("\nFichas da banca: %d", banca.fichas);
        printf("\nFichas de %s: %d", player[p1].nome, player[p1].fichas);
        printf("\nFichas de %s: %d", player[p2].nome, player[p2].fichas);

        printf("\n\nQuer continuar jogando?  (Sim - 1/ Nao - 0) ");
        scanf("%d", &continuar);
        system("cls");

        if(!continuar)
            jogar = 0;

       }while(jogar);

       atualiza_ranking();
}

#endif // PLAYERSFUNCTIONS_H_INCLUDED
