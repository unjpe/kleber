#ifndef RANKINGFUNCTIONS_H_INCLUDED
#define RANKINGFUNCTIONS_H_INCLUDED


int ranking(){
    int i = 0;

    printf("RANKING\nPosicao\tNome\t\tFichas\n");
    while(i < 10){
        printf("%d\t", i+1);
        printf("%s\t\t", player[i].nome);
        printf("%d\n", player[i].fichas);
        //printf("\n");
        i++;
    }
    return 0;
}

void carregar_ranking(){
    int i = 0, x = 0, y = 0, vetfichas[11] = {0};
    char linha[50];
    char nomes[10][50];
    FILE* arquivo = NULL;

	char * parte_nome;
	char * parte_vitorias;
	arquivo = fopen("ranking.txt", "a+");
	if(arquivo == NULL){
		printf("Nao foi possivel abrir o arquivo");
        return 0;
    }

	while(fgets(linha, 50, arquivo)) {
		parte_nome = strtok(linha, "\t");
		parte_vitorias = strtok(NULL, "\t");
		strcpy(nomes[i], parte_nome);
        int fichas = atoi(parte_vitorias);
        vetfichas[i] = fichas;
        i++;
	}
    fclose(arquivo);

    for(i = 0; i < 10; i++){
        strcpy(player[i].nome, nomes[i]);
        player[i].fichas = vetfichas[i];
    }
}



void atualiza_ranking(){
    int x = 0, y = 0;
    FILE* arquivo = NULL;

    for(x=0; x<12; x++)
        for(y=x+1; y<12; y++)
            if(player[x].fichas < player[y].fichas)
            {
                temp = player[x];
                player[x] = player[y];
                player[y] = temp;
            }

    arquivo = fopen("ranking.txt", "w");
    for(int i=0;i<10;i++)
        fprintf(arquivo, "%s\t%d\n", player[i].nome, player[i].fichas);
    fclose(arquivo);
}


#endif // RANKINGFUNCTIONS_H_INCLUDED
