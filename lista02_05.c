#include <stdio.h>

int main(){
    int n, r, aux = 0;
    printf("Digite o numero de termos da P.A.: ");
    scanf("%d", &n);
    printf("Digite a razao da P.A.: ");
    scanf("%d", &r);
    printf("\nOs %d elementos da P.A. com razao %d e termo inicial zero sao: \n", n, r);
    for(int i = 0; i < n; i++){
        printf("%d\n", aux);   //como queremos comecar do zero, imprimimos antes de incrementar.
        aux += r;
    }
    return 0;
}
