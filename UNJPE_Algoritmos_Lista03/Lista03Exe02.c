#include <stdio.h>

int main (){
    int i, cont = 0;
    char nome1[100 + 1];
    char nome2[100 + 1];
    printf("###### Programa STRCMP: Compara 2 strings retornando 0 se são iguais #####\n\n");
    printf("Entre com a primeiro string (Max. 100 caracteres): ");
    gets(nome1);
    printf("Entre com a segunda string (Max. 100 caracteres): ");
    gets(nome2);
    for(i = 0; nome1[i] != '\0' || nome2[i] != '\0'; i++){
        if(nome1[i] != nome2[i]){
           cont++;
           break;
        }
    }
    if(cont){
        printf("\nRetorno = %d\n", cont);
        printf("As strings sao diferentes\n");
    } else {
        printf("\nRetorno = %d\n", cont);
        printf("As strings sao iguais\n");
    }
    return 0;
}