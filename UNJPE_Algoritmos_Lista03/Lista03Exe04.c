#include <stdio.h>

int main(){
    int i = 0;
    char str[100];
    printf("###### Programa STRLEN: Retorna quantidade de caracteres de uma string #####\n\n");
    printf("Digite uma palavra ou frase: ");
    gets(str);
    for(i = 0; str[i] != '\0'; i++);
    printf("O numero de caracteres eh: %d\n", i);
    return 0;
}
