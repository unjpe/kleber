#include <stdio.h>

int main (){
    int i = 0;
    char nome1[100 + 1];
    char nome2[100 + 1];
    printf("###### Programa STRCPY: Copia o conteudo de uma string para outra #####\n\n");
    printf("Entre com uma string (Max. 100 caracteres): ");
    gets(nome1);
    while(nome1[i] != '\0'){
        nome2[i] = nome1[i];
        i++;
    }
    nome2[i] = '\0'; //garante que o final de nome2 seja fim de linha
    printf("Primeira string: %s\n", nome1);
    printf("Segunda string: %s\n", nome2);
    return 0;
}