#include <stdio.h>

int main (){
    int i, len = 0;
    char nome1[200 + 1];
    char nome2[100 + 1];
    printf("###### Programa STRCAT: Concatena duas strings #####\n\n");
    printf("Entre com a primeiro string (Max. 100 caracteres): ");
    gets(nome1);
    printf("Entre com a segunda string (Max. 100 caracteres): ");
    gets(nome2);
    for(i = 0; nome1[i] != '\0'; i++);
    len = i;
    i = 0;
    do{
        nome1[i + len] = nome2[i];
        i++;
    }while(nome2[i] != '\0');
    nome1[i + len] = '\0'; //garante que o final de nome1 seja fim de linha
    printf("A concatenacao das strings eh: %s\n", nome1);
    return 0;
}