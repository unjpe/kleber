#include <stdio.h>

int main (){
    int i = 0, len = 0, flag = 0;
    char nome[100 + 1];
    printf("###### Programa PALINDROMO: verifica se uma string é palindromo #####\n\n");
    printf("Entre com uma string (Max. 100 caracteres): ");
    gets(nome);
    // Transformando maiusculas em minusculas
    for(char *p = nome; *p; ++p)
        *p = *p > 0x40 && *p < 0x5b ? *p | 0x60 : *p;
    for(len = 0; nome[len] != '\0'; len++);
    // Verifica se inicio da string eh igual ao fim
    while(i<len/2){
        if(nome[i] != nome[len - i - 1]){
            flag++;
            break;
        }
        i++;
    }
    if(flag){
        printf("O nome %s nao eh palindromo\n", nome);
    } else{
        printf("O nome %s eh palindromo\n", nome);
    }
    return 0;
}
