/* URI Online Judge | 1871: Zero vale Zero
Cada caso de teste inicia com dois inteiros M e N.
O último caso de teste é indicado quando N = M = 0,
sendo que este caso não deve ser processado.*/

#include <stdio.h>

int main(){
	int M, N, soma, array[21], i;

	while((M != 0 || N != 0)){
		scanf("%d %d",&M, &N);
		soma = M + N;

		for(i = 0; i < 21; i++){
			array[i] = soma %10;
			soma /= 10;
		}

		for(i = 20; i >= 0; i--){
			if(array[i] != 0){
				printf("%d", array[i]);
			}
		}
		printf("\n");
	}
    return 0;
}
