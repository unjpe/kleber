/* URI Online Judge | 2157: Sequência Espelho
A entrada possui um valor inteiro C indicando a quantidade de casos de teste.
Em seguida, cada caso apresenta dois valores inteiros, B e E, indicando o início e o fim da sequência.
Para cada caso de teste, o programa imprime a sequência espelho correspondente.*/

#include <stdio.h>
#include <string.h>

int main() {

    int n, ini, fim, i, k, p;
    char c[200];
    scanf("%d", &n);

    for(i=0; i = n; --n){
        scanf("%d%d", &ini, &fim);
        for(i = ini; i <= fim; i++)
            printf("%d", i);
            for(i = fim; i >= ini; i--){
                sprintf(c, "%d", i);
                p = strlen(c);
                for (k = p; k > 0; k--){
                    printf("%c", c[k-1]);
                }
            }
        printf("\n");
    }
    return 0;
}
