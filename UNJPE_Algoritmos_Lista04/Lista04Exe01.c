/*
URI Online Judge | 1000
Hello World!

Imprimir a mensagem "Hello World!".
*/

#include <stdio.h>

int main() {
  printf("Hello World!\n");
  return 0;
}
