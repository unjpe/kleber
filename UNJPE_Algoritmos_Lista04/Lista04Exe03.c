/* Uri 1151 - Fibonacci Fácil
Le um inteiro n e retorna sequecia de fibonacci
de tamanho n, conmeçando em zero.*/

#include<stdio.h>

int main(){
    int n, i = 0, fibo = 0, fibom1 = 0, fibom2 = 0;
    scanf("%d", &n);

    while(i < n){
        if(i == 1){
            fibo = 1;
        }
        else if(i > 1) {
            fibom2 = fibom1;
            fibom1 = fibo;
            fibo = fibom1 + fibom2;
        }

        if(i < n-1)
            printf("%d ", fibo);
        else
            printf("%d\n", fibo);
        i++;
    }
    return 0;
}
