/* URI Online Judge | 1803: Matring
A primeira e última coluna de uma matring guarda a chave para traduzi-la na mensagem original. 
As colunas restantes de uma matring representam uma string codificada em ASCII, uma coluna por caractere. 
Cada coluna é lida como um número de 4 dígitos; 
uma sequência de dígitos de cima para baixo é o mesmo que uma sequência de dígitos da esquerda para a direita na horizontal.
Seja o primeiro número F, o último número L e os restantes uma sequência de números Mi, onde 1 ≤ i ≤ N. 
A primeira coluna de uma matring é indexada por zero.
Para decodificar, calculamos: Ci = (F * Mi + L) mod 257, onde Ci é o caractere em ASCII na posição i da mensagem original.
A entrada é uma matring, ou seja, uma matriz 4x(N+2) de dígitos (de 0 a 9) com 0 < N < 80.
A saída é dada em uma única linha e corresponde a string decodificada. Inclua o caractere de fim-de-linha após a string.
*/

#include <stdio.h>
#include <string.h>

int main(){
    int i, j, N, F, L, M;
    char matriz[4][100], C;

    for(i = 0; i < 4; i++){
        scanf("%s", matriz[i]);
    }
    N = strlen(matriz[0]) - 2;

    for(i = 0, F = 0, L = 0; i < 4; i++){
        F = F * 10 + matriz[i][0] - '0';
        L = L * 10 + matriz[i][N + 1] - '0';
    }

    for(j = 1; j <= N; j++){
        for(i = 0, M = 0; i < 4; i++){
            M = M * 10 + matriz[i][j] - '0';
        }
        C = (F * M + L) % 257;
        printf("%c", C);
    }
    printf("\n");
    return 0;
}
