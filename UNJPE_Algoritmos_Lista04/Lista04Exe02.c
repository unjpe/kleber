/* URI Online Judge | 2157 - Gasto de Combustivel
Le dois inteiros e calcula a distancia maxima
que o carro pode percorrer com três casas decimais
a partir do rendimento = 12 km/L*/

#include <stdio.h>

int main(){
    int tempo, velocidade;
    float distancia, rendimento = 12.0;
    scanf("%d", &tempo);
    scanf("%d", &velocidade);

    distancia = tempo * velocidade / rendimento;

    printf("%.3f\n", distancia);
    return 0;
}
