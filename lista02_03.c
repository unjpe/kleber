#include <stdio.h>

int main(){
	unsigned int n;
	unsigned long aux = 1; // Mesmo com unsigned long so da pra achar corretamente 12!.
	printf("Digite um numero positivo: ");
	scanf("%d", &n);
	for(int i=n; i>1; i--){
        aux *= i;
	}
    printf("O fatorial de %u eh %u\n", n, aux);
	return 0;
}