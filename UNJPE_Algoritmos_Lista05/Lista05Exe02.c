/*
Escreva um trecho de codigo em C para fazer a criação dos novos tipos de dados conforme solicitado abaixo:
Horario: composto de hora, minutos e segundos.
Data: composto de dia, mes e ano.
Compromisso: composto de uma data, horario e texto que descreve o compromisso.
*/

#include<stdio.h>
//Declarando estrutura Horario
struct Horario{
    int hora;
    int minutos;
    int segundos;
};
//Declarando estrutura Data
struct Data{
    int dia;
    int mes;
    int ano;
};
//Declarando estrutura Compromisso que contém as estruturas horario e data e um texto
struct Compromisso{
    struct Horario horario;
    struct Data data;
    char *texto;
};

int main(){
    //Inicializando uma estrutura Compromisso com nome agenda 
    struct Compromisso agenda;
    //Passando dados da agenda
    agenda.data.dia = 23;
    agenda.data.mes = 02;
    agenda.data.ano = 2020;

    agenda.horario.hora = 12;
    agenda.horario.minutos = 15;
    agenda.horario.segundos = 07;

    agenda.texto = "Medico";
    //Imprimindo
    printf("Compromisso: %s\n", agenda.texto);
    printf("Data: %d/%d/%d\n", agenda.data.dia, agenda.data.mes, agenda.data.ano);
    printf("Horario: %d:%d:%d\n", agenda.horario.hora, agenda.horario.minutos, agenda.horario.segundos);

    return 0;
}
