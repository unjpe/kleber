/*
Uma empresa comercial possui um programa para controle das receitas e despesas em seus 10 projetos. 
Os projetos sao numerados de 0 ate 9. 
Faca um programa C que controle a entrada e saida de recursos dos projetos. 
O programa devera ler um conjunto de informacoes contendo:
 Numero do projeto, valor, tipo despesa ("R" - Receita e "D" - Despesa). 
O programa termina quando o valor do codigo do projeto for igual a -1. 
Sabe-se que Receita deve ser somada ao saldo do projeto e despesa subtraida do saldo do projeto. 
Ao final do programa, imprimir o saldo final de cada projeto.
*/

#include<stdio.h>

struct Codigo {
	int numero;
	float valor;
	char tipo;
} codigo;

int main(){
	float projetos[10] = {0};
	int i;


	printf("Digite o codigo do projeto (-1 para sair): ");
	scanf("%d", &codigo.numero);

	while (codigo.numero != -1) {
		printf("Digite o Valor : ");
		scanf("%f", &codigo.valor);

		printf("Digite o tipo de transacao (R ou D): ");
		getchar();
		scanf("%c", &codigo.tipo);

		if (codigo.tipo == 'R' || codigo.tipo == 'r') {
			projetos[codigo.numero] = projetos[codigo.numero] + codigo.valor;
		}
		else {
			if (codigo.tipo == 'D' || codigo.tipo == 'd') {
				projetos[codigo.numero] = projetos[codigo.numero] - codigo.valor;
			}
			else {
				printf("Tipo Invalido !!");
			}
		}
		printf("Digite o codigo do projeto (-1 para sair): ");
		scanf("%d", &codigo.numero);
	}

    printf("\n");
	for (i = 0; i < 10; i++){
		printf("Saldo do projeto %d = R$ %.2f\n", i, projetos[i]);
	}
	return 0;
}
