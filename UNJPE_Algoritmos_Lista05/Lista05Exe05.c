/*
Considere a estrutura. Faça um programa com os requisitos:
a) Cria uma conta
b) Consulta o saldo do cliente (entra com o numero da conta conferindo apenas a senha)
c) Deposita um valor (entra com o numero da conta e confere o nome do cliente)
d) Saca um valor (entra com o numero da conta e confere senha e chave, o cliente tem apenas autorização de sacar o seu dinheiro, conta sem limite)
e) Encerre a conta ( entra com o numero da conta e confere senha e chave, faça uma pergunta para que o cliente confira a operação e apague seus dados)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct cadastro_cliente{
	char nome[50];
	int num_conta;
	int senha;
	char chave[3];
	float saldo;
}cad_cliente[100];

int maxconta = 0;

int criar_conta();
int consultar_saldo();
int deposito();
int saque();
int listagem();
int encerrar_conta();

int main(){
    int op = -1, id = 0, retorno, i;

    for(i = 0; i < 100; i++)
        strcpy(cad_cliente[i].nome, "");

    do{
        printf("-----------------------------------------------------\n");
        printf("Bem-Vindo ao Banco Ciencias da Computacao - UNIPE!!!\n\n");
        printf("Escolha a operacao desejada: \n");
        printf("1 - Criar conta\n");
        printf("2 - Consultar saldo\n");
        printf("3 - Deposito\n");
        printf("4 - Saque\n");
        printf("5 - Listagem\n");
        printf("6 - Encerrar conta\n");
        printf("0 - sair\n");
        printf("-----------------------------------------------------\n");

        scanf("%d", &op);


        switch(op){
            case 0:
                system("cls");
                printf("Fim do programa\n");
                break;
            case 1:
                system("cls");
                criar_conta();
                break;
            case 2:
                system("cls");
                consultar_saldo();
                break;
            case 3:
                system("cls");
                deposito();
                break;
            case 4:
                system("cls");
                saque();
                break;
            case 5:
                system("cls");
                //printf("listagem();\n");
                listagem();
                break;
            case 6:
                system("cls");
                //printf("encerrar_conta();\n");
                encerrar_conta();
                break;
            default:
                system("cls");
                printf("Operacao Invalida!\n");
        }
    } while(op);

    return 0;
}


int criar_conta(){
    char nome[100], chave[20];
    int numconta = 0, senha = 0;

    while(numconta == cad_cliente[numconta].num_conta){
        numconta++;
    }
    maxconta = numconta;

    printf("Nome do cliente: ");
    scanf("%s", &nome);
    printf("Digite uma senha (4 numeros): ");
    scanf("%d", &senha);
    printf("Digite uma chave (3 letras): ");
    scanf("%s", &chave);

    strcpy(cad_cliente[numconta].nome, nome);
    cad_cliente[numconta].num_conta = numconta;
    cad_cliente[numconta].senha = senha;
    strcpy(cad_cliente[numconta].chave, chave);
    cad_cliente[numconta].saldo = 0.0;
    printf("Conta cadastrada com sucesso!!!\n\n");
    printf("Nome do cliente:\t%s\n", cad_cliente[numconta].nome);
    printf("Numero da conta:\t%d\n", cad_cliente[numconta].num_conta);

    return 0;
}


int listagem(){
    int numconta = 1;
    while(numconta <= maxconta){
        printf("Nome do cliente:\t%s\n", cad_cliente[numconta].nome);
        printf("Numero da conta:\t%d\n", cad_cliente[numconta].num_conta);
        printf("Senha:\t%d\n", cad_cliente[numconta].senha);
        printf("Chave:\t%s\n", cad_cliente[numconta].chave);
        printf("Saldo:\tR$ %.2f\n", cad_cliente[numconta].saldo);
        printf("\n");
        numconta++;
    }
    return 0;
}


int consultar_saldo(){
    int numconta = 1, senha = 0;

    printf("Digite o numero da conta: ");
    scanf("%d", &numconta);

    printf("Digite a senha: ");
    scanf("%d", &senha);

    if(senha != cad_cliente[numconta].senha){
        printf("Senha invalida!\n");
        return 0;
    } else{
        printf("\nNome do cliente:\t%s\n", cad_cliente[numconta].nome);
        printf("Numero da conta:\t%d\n", cad_cliente[numconta].num_conta);
        printf("Saldo:\tR$ %.2f\n", cad_cliente[numconta].saldo);
    }
    return 0;
}

int deposito(){
    char nome[100];
    int numconta = 1, resp = 0;
    float valor = 0.0;

    printf("Digite o numero da conta: ");
    scanf("%d", &numconta);
    printf("\nO nome do cliente eh:\t%s? (1-SIM 2-NAO):", cad_cliente[numconta].nome);
    scanf("%d", &resp);


    if(resp != 1){
        printf("Tente outra vez!\n");
        return 0;
    }

    printf("Entre com valor de deposito: ");
    scanf("%f", &valor);
    cad_cliente[numconta].saldo += valor;
    printf("Valor depositado: R$ %.2f\n", valor);
    return 0;
}


int saque(){
    char chave[20];
    int numconta = 1, senha;
    float valor = 0.0;

    printf("Digite o numero da conta: ");
    scanf("%d", &numconta);
    printf("Digite uma senha: ");
    scanf("%d", &senha);
    printf("Digite uma chave: ");
    scanf("%s", &chave);

    if(cad_cliente[numconta].senha != senha){
        printf("Senha Incorreta!");
        return 0;
    } else if(strcmp(chave, cad_cliente[numconta].chave) != 0){
        printf("Chave Incorreta!");
        return 0;
    } else{
        printf("Entre com valor de saque: ");
        scanf("%f", &valor);

        if(valor > cad_cliente[numconta].saldo){
            printf("Saldo insuficiente\n");
            return 0;
        } else{
            cad_cliente[numconta].saldo -= valor;
        }
    }
    return 0;
}

int encerrar_conta(){
    char chave[20];
    int numconta = 1, senha, resp = 0;

    printf("Digite o numero da conta: ");
    scanf("%d", &numconta);
    printf("Digite uma senha: ");
    scanf("%d", &senha);
    printf("Digite uma chave: ");
    scanf("%s", &chave);

    if(cad_cliente[numconta].senha != senha){
        printf("Senha Incorreta!");
        return 0;
    } else if(strcmp(chave, cad_cliente[numconta].chave) != 0){
        printf("Chave Incorreta!");
        return 0;
    } else{
        printf("Deseja realmente finalizar a conta? (1-SIM 2-NAO): ");
        scanf(" %d", &resp);

        if(resp != 1){
            printf("Muito bem! Continuaremos juntos!");
            return 0;
        }else{
            cad_cliente[numconta] = cad_cliente[101];
            printf("Conta encerrada com sucesso!!!\n");
            return 0;
        }

    }
}